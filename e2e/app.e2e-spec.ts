import { Rcv2Page } from './app.po';

describe('rcv2 App', () => {
  let page: Rcv2Page;

  beforeEach(() => {
    page = new Rcv2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('rcv2 works!');
  });
});
