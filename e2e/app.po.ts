import { browser, element, by } from 'protractor';

export class Rcv2Page {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('rcv2-root h1')).getText();
  }
}
