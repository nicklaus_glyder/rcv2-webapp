import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HbaseSourceComponent } from './hbase-source/hbase-source.component';
import { HbaseQueryComponent } from './hbase-query/hbase-query.component';

import { HbaseService } from './services/hbase.service';
import { KeyValPipe } from './keyVal.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HbaseSourceComponent,
    HbaseQueryComponent,
    KeyValPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [HbaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
