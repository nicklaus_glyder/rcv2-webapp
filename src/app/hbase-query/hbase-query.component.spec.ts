import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HbaseQueryComponent } from './hbase-query.component';

describe('HbaseQueryComponent', () => {
  let component: HbaseQueryComponent;
  let fixture: ComponentFixture<HbaseQueryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HbaseQueryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HbaseQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
