import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, animate, transition} from '@angular/animations';
import { Table } from '../models/table'
import { HbaseService } from '../services/hbase.service';
import { ResultSet } from '../models/result-set';

@Component({
  selector: 'rcv2-hbase-query',
  templateUrl: './hbase-query.component.html',
  styleUrls: ['./hbase-query.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('in', style({transform: 'translateX(0)'})),
      transition('void => *', [
        style({transform: 'translateX(100%)'}),
        animate("300ms ease-in")
      ]),
      transition('* => void', [
        animate("300ms ease-out", style({transform: 'translateX(100%)'}))
      ])
    ])
  ]
})
export class HbaseQueryComponent implements OnInit {
  @Input() selectedTable: Table;
  resultNum: number;
  query: string;
  ranking: string;
  goodQuery: boolean;
  results: ResultSet;

  constructor(private hbase: HbaseService) { }

  ngOnInit() {
    this.resultNum = 20;
    this.ranking = "cosine";
    this.goodQuery = true;
  }

  search() {
    if(!this.query || this.query === "" || !this.selectedTable) {
      this.goodQuery = false;
    } else {
      this.goodQuery = true;
      this.hbase.queryTable(this.selectedTable,
                            this.query,
                            this.resultNum,
                            this.ranking).subscribe((res) => {
        this.results = res
      })
    }
  }

  clear() {
    this.query = null;
    this.results = null;
  }

}
