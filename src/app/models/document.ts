export class Document {
  rank:       number;
  docID:      string;
  termVector: any;
  title:      string;

  constructor () {
    this.rank = 0;
    this.docID = "";
    this.termVector = {};
    this.title = "";
  }
}
