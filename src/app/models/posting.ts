export class Posting {
  term:   string;
  docID:  number;
  ld:     number;
  tf:     number;
  idf:    number;
  ldLa:   number;

  constructor(obj: {}) {
    for (var prop in obj) this[prop] = obj[prop];
  }
}
