import {Document} from './document';

export class ResultSet {
  documents: Document[];
  queryVector: any;
  total: number;

  constructor() {

  }
}
