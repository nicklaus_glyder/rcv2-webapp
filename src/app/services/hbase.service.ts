import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Table } from '../models/table'
import { Document } from '../models/document';
import { ResultSet } from '../models/result-set';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class HbaseService {

  constructor(private http: Http) { }

  getTables(): Observable<Table[]> {
    return this.http.get("api/tables").map((res) => {
      let body = res.json();
      return body.table;
    })
  }

  queryTable(table: Table,
             query: string,
             resultNum: number,
             ranking: string): Observable<ResultSet> {
    let payload = {query: query, num: resultNum, ranking: ranking}
    return this.http.post("api/table/"+table.name, payload).map((res) => {
      return res.json();
    });
  }

}
