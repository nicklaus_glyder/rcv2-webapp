import { Component, OnInit } from '@angular/core';
import { Table } from './models/table';

@Component({
  selector: 'rcv2-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  selectedTable: Table;

  ngOnInit() {
  }

  onNotify(payload) {
    this.selectedTable = payload;
  }
}
