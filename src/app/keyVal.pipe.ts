import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'keyVal'})

export class KeyValPipe implements PipeTransform {
    transform(value: any, args?: any[]): Object[] {
        // create instance vars to store keys and final output
        let keyArr: any[] = Object.keys(value),
            dataArr = [];

        // loop through the object,
        // pushing values to the return array
        keyArr.forEach((key: any) => {
            let obj = {key: key, value: value[key]}
            dataArr.push(obj);
        });

        // return the resulting array
        return dataArr;
    }
}
