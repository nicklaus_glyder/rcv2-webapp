import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HbaseSourceComponent } from './hbase-source.component';

describe('HbaseSourceComponent', () => {
  let component: HbaseSourceComponent;
  let fixture: ComponentFixture<HbaseSourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HbaseSourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HbaseSourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
