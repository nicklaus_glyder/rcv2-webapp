import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { HbaseService } from '../services/hbase.service';

import { Table } from '../models/table'

@Component({
  selector: 'rcv2-hbase-source',
  templateUrl: './hbase-source.component.html',
  styleUrls: ['./hbase-source.component.scss']
})
export class HbaseSourceComponent implements OnInit {
  tables: Table[];
  selectedTable: Table;
  @Output() notify: EventEmitter<Table> = new EventEmitter<Table>();

  constructor(private hbase: HbaseService) { }

  ngOnInit() {
    console.log("Tables API call...")
    this.hbase.getTables().subscribe((tables) => {
      this.tables = tables;
    })
  }

  selectTable(table: Table) {
    this.selectedTable = table;
    this.notify.emit(this.selectedTable);
  }

}
