import {Document} from '../src/app/models/document';
import {Posting} from '../src/app/models/posting';

export class Ranking {
  // BM 25 scoring function
  private static bm_25_score(posting: Posting) {
    let k1 = 1.5;
    let b = 0.75;
    let bm25Num = (k1+1)*posting.tf;
    let bm25Den = k1*((1-b) + b*(posting.ldLa))+posting.tf;
    return posting.idf * (bm25Num/bm25Den);
  }

  // Simple tf idf scoring algorithm
  private static tf_idf_score(posting: Posting) {
    return posting.tf * posting.idf;
  }

  // Parse HBase result set into Document map
  private static build_documents(terms: string[], resultSet: any, scoreFunction: Function) {
    let documents = {};
    // For each term in our query, add tf-idf to documents
    terms.forEach((term) => {
      // Find the result for this term
      let result = resultSet.filter(function( obj ) { return obj.row.toString() === term; })[0];

      if(result) {
        // Build document list+vectors and query vector
        result.columns.forEach((col) => {
          if (col.family.toString() === "docIDs") {
            let posting = new Posting(JSON.parse(col.value.toString()))
            if (!documents[posting.docID]) {
              documents[posting.docID] = new Document();
              documents[posting.docID].docID = posting.docID.toString();
            }
            documents[posting.docID].termVector[term] = scoreFunction(posting);
          }
        });
      }

    });
    return documents;
  }

  private static build_query_vector(terms: string[], resultSet: any) {
    let queryVector = {};
    // For each term in our query, add tf-idf to documents
    terms.forEach((term) => {
      // Find the result for this term
      let result = resultSet.filter(function( obj ) { return obj.row.toString() === term; })[0];

      if(result) {
        // Build document list+vectors and query vector
        result.columns.forEach((col) => {
          if (col.family.toString() === "docIDs") {
            let posting = new Posting(JSON.parse(col.value.toString()))
            queryVector[term] = posting.idf;
          }
        });
      }

    });
    return queryVector
  }

  // Normalize a given vector of values
  private static normalize_vector(vector: {}) {
    let vLength = Math.sqrt(Object.keys(vector).reduce((acc, curr) => {
      return acc + Math.pow(vector[curr], 2)
    }, 0));
    Object.keys(vector).map((key, index) => {
      vector[key] = vector[key]/vLength;
    });
  }

  /* Perform BM25 ranking of documents */
  public static bm_25_rank(terms: string[], resultSet: any, resultNum: number) {
    let documents = Ranking.build_documents(terms, resultSet, Ranking.bm_25_score);
    let queryVector = Ranking.build_query_vector(terms, resultSet);

    // Normalize document vectors and cosine rank them
    Object.keys(documents).forEach((docID) => {
      // Document vector length, then normalize
      let docVector = documents[docID].termVector;

      // BM 25 Score
      documents[docID].rank = 0.0;
      Object.keys(docVector).forEach((term) => {
        documents[docID].rank += docVector[term];
      });
    });

    // Sort and select top X documents to send as response
    let docArray = [];
    Object.keys(documents).map((docID) => {
      docArray.push(documents[docID]);
    });

    // Create result object
    let queryRankResult = {};
    queryRankResult['queryVector'] = {};
    queryRankResult['total'] = docArray.length;

    // Kepp top X documents
    let ranking = docArray.sort((a,b) => {
      return b.rank - a.rank;
    });
    ranking.splice(resultNum);
    queryRankResult['documents'] = ranking;

    return queryRankResult;
  }

  /* Perform cosine similarity ranking of documents */
  public static cosine_rank(terms: string[], resultSet: any, resultNum: number) {
    let documents = Ranking.build_documents(terms, resultSet, Ranking.tf_idf_score);
    let queryVector = Ranking.build_query_vector(terms, resultSet);
    Ranking.normalize_vector(queryVector);

    // Normalize document vectors and cosine rank them
    Object.keys(documents).forEach((docID) => {
      // Document vector length, then normalize
      let docVector = documents[docID].termVector;
      Ranking.normalize_vector(docVector);

      // Cosine similarity
      let rank = 0.0;
      Object.keys(queryVector).forEach((term) => {
        let qScore = queryVector[term];
        // Remember, the document might not contain query term
        let dScore = docVector.hasOwnProperty(term) ? docVector[term] : 0.0;
        rank += qScore * dScore;
      });

      // Set rank
      documents[docID].rank = rank;
    });

    // Sort and select top X documents to send as response
    let docArray = [];
    Object.keys(documents).map((docID) => {
      docArray.push(documents[docID]);
    });

    // Create result object
    let queryRankResult = {};
    queryRankResult['queryVector'] = queryVector;
    queryRankResult['total'] = docArray.length;

    // Kepp top X documents
    let ranking = docArray.sort((a,b) => {
      return b.rank - a.rank;
    });
    ranking.splice(resultNum);
    queryRankResult['documents'] = ranking;

    return queryRankResult;
  }
}
