import express = require('express');
let router = express.Router();
import http = require('http');

import hbase = require("hbase-rpc-client");
import protobuf = require("protobufjs");

import {Ranking} from "../ranking";

/* Connect to Zookeeper to query HBase REST Server */
let client = hbase({
  zookeeperHosts: ["localhost"],
	zookeeperRoot: "/hbase",
	zookeeperReconnectTimeout: 20000,
	rpcTimeout: 30000,
	callTimeout: 5000,
	tcpNoDelay: false,
	tcpKeepAlive: false
});

client.on("error", (err) => {
	console.log("hbase client error:", err);
});

/* GET api listing. */
router.get('/', (req, res) => {
  res.send(`
    RCV2 API:
    <p>GET /api/tables - List of avaiable HBase Tables</p>
    <p>POST /api/:table - Body data: <br>
      <ul>
        <li>num : number - of results to return</li>
        <li>query : string - query terms</li>
        <li>ranking : string : [cosine|bm25] - ranking to be performed</li>
      </ul>
    </p>
  `);
});

/* GET list of available tables from HBase Rest Server */
router.get('/tables', (req, res) => {
  http.get({
    hostname: 'localhost',
    port: process.env.HBASE_REST_PORT,
    path: '/',
    headers: {"accept":"application/json"}
  }, (json) => {
    json.on("data", function(data) {
      res.setHeader('Content-Type', 'application/json');
      res.send(data);
    });
  });
});

/* POST query, process resultSet server side and return ranked documents */
router.post('/table/:table', (req, res) => {
  try {
    let query = req.body.query;
    let terms = query.split(' ');
    let table = req.params.table;
    let num = req.body.num ? req.body.num : 20;

    // Query HBase REST server
    client.mget(table, terms, (err, result) => {
      // Filter out any terms that didn't have an entry in the DB
      let resultSet = result.filter((obj) => obj.hasOwnProperty('row'));

      // Do ranking
      let rankedDocs;
      switch (req.body.ranking) {
        case "cosine":  rankedDocs = Ranking.cosine_rank(terms, resultSet, num); break;
        case "bm25":    rankedDocs = Ranking.bm_25_rank(terms, resultSet, num); break;
        default:        rankedDocs = Ranking.cosine_rank(terms, resultSet, num); break;
      }

      // Add article titles to ranked documents
      let ids = rankedDocs.documents.map((doc) =>  { return doc.docID; });
      if (ids.length > 0) {
      	client.mget("TitleHBase", ids, (err, titles) => {
          titles.forEach((title) => {
            let id = title.row.toString();
            let t = title.cols['title:title'].value.toString();
            rankedDocs.documents.filter((obj) => obj.docID === id)[0].title = t;
          });
          // Send document JSON back to Angular app
          res.json(rankedDocs);
        });
      }
    });
  } catch (e) { res.sendStatus(401); }
});

module.exports = router;
