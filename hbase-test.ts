console.log("Starting hbase client")

var hbase = require("hbase-rpc-client")
var protobuf = require("protobufjs");

var client = hbase({
  zookeeperHosts: ["localhost"],
	zookeeperRoot: "/hbase",
	zookeeperReconnectTimeout: 20000,
	rpcTimeout: 30000,
	callTimeout: 5000,
	tcpNoDelay: false,
	tcpKeepAlive: false
});

client.on("error", (err) => {
	console.log("hbase client error:", err);
});

var get = new hbase.Get('alaska');

client.get('UniWordHBase', get, (err, res) => {
  res.columns.forEach(col => {
      var fam = col['family'].toString('ascii');
      if(fam === "docIDs") {
        var b = new Buffer(col['qualifier'])
        console.log(b.readIntBE(0,b.length))      
      }
  });
})
