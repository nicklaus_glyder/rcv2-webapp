# README #

This is the NodeJS interface for the HBase index tables produced by te indexer. 

### What is this repository for? ###

* Submit search queries in a webapp gui
* 1.0.0

### How do I get set up? ###

* run npm install
* NodeJS 6+, NPM 4+
* Apache HBase 1.2.4
